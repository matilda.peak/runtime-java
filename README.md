---
Title:      MatildaPeak base Docker image
Author:     Alan Christie
Date:       23 June 2018
---

[![build status](https://gitlab.com/matilda.peak/runtime-java/badges/master/build.svg)](https://gitlab.com/matilda.peak/runtime-java/commits/master)

# A base image for our Java runtime
This image is used by our Java applications, it provides a common base
for the application run-time. If your application is Java-based then
you should be using this image as a `FROM`.
