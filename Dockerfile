# ----------------------------------------------------------------------------
# Copyright (C) 2018 MatildaPeak - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# ----------------------------------------------------------------------------

FROM python:3.6.6

# -----------------------------------------------------------------------------

ENV HOME /root

RUN apt-get purge -y --auto-remove \
        wget \
        bzr \
        git \
        mercurial \
        openssh-client \
        subversion \
        autoconf \
        automake \
        patch \
        xz-utils \
    && rm -rf /var/lib/apt/lists/*

# JRE -------------------------------------------------------------------------

ENV JAVA_HOME /opt/java-1.8
ENV PATH $JAVA_HOME/bin:$PATH

COPY library/server-jre-8u181-linux-x64.tar.gz /tmp/

RUN tar -zxf /tmp/server-jre-8u181-linux-x64.tar.gz -C /tmp \
    && mv /tmp/jdk1.8.0_181/jre $JAVA_HOME \
    && rm /tmp/server-jre-8u181-linux-x64.tar.gz \
    && rm -rf /tmp/jdk1.8.0_181

# Don't delete any file from oracle: "Oracle grants you a non-exclusive,
# non-transferable, limited license without fees to reproduce and distribute
# the Software, provided that (i) you distribute the Software complete and
# unmodified and only bundled as part of, and for the sole purpose of running,
# your Programs."

WORKDIR $HOME
